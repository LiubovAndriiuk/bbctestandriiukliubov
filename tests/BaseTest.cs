﻿using System;
using BBCAutomationTesting.pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace BBCAutomationTesting.tests
{
    class BaseTest
    {
        protected IWebDriver driver;

        [SetUp]
        public void StartTest()
        {
            this.driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl("https://www.bbc.com");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

        }
        [TearDown]
        public void CloseTest()
        {
            driver.Quit();
        }

        public IWebDriver GetWebDriver()
        {
            return driver;
        }
        public BasePage GetBasePage()
        {
            return new BasePage(driver);
        }
        public HomePage GetHomePage()
        {
            return new HomePage(driver);
        }
        public NewsPage GetNewsPage()
        {
            return new NewsPage(driver);
        }
    }
}