﻿

namespace BBCAutomationTesting.tests
{
    internal class DataTest
    {
        public static readonly string expectedTextHeadingArticle = "HSBC moved scam millions, big banking leak shows";
        public static readonly string[] expectedTextSecondaryTitlesArticle = {     
                                          "Lionel Messi wins fight to trademark his logo",
                                          "Covid pushes New Zealand into worst recession",
                                          "Protesters topple conquistador statue in Colombia",
                                          "Manhunt for 'naked' prisoners in Ugandan hills",
                                          "Australia ex-PM hacked after Instagramming boarding pass" ,
                                          "Police 'requested heat ray' for White House protest",
                                          "Nintendo 3DS discontinued after almost a decade",
                                          "Kanye posts photos of his record deal amid dispute"};

    }
}
