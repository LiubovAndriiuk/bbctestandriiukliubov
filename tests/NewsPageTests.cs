﻿using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;

namespace BBCAutomationTesting.tests
{
    [TestFixture]
    class NewsPageTests : BaseTest
    {
        [SetUp]
        public void StartUp()
        {
            PageFactory.InitElements(driver, this);
        }

        [Test]
        public void CheckTheNameOfTheHeadlineArticle()
        {
            GetHomePage().ClickOnTheNewsHeading();
            GetNewsPage().ClickOnThePopUpSingIn();
            GetNewsPage().GetTextHeaderArticle();
            Assert.AreEqual(DataTest.expectedTextHeadingArticle, GetNewsPage().GetTextHeaderArticle());
        }
        [Test]
        public void CheckSecondaryArticleTitles()
        {
            GetHomePage().ClickOnTheNewsHeading();
            GetNewsPage().ClickOnThePopUpSingIn();
            GetNewsPage().GetSecondaryTitleArticle();
            Assert.AreNotEqual(DataTest.expectedTextHeadingArticle, GetNewsPage().GetSecondaryTitleArticle());

        }
        [Test]
        public void ThirdTest()
        {
            GetHomePage().ClickOnTheNewsHeading();
            GetNewsPage().ClickOnThePopUpSingIn();
            GetNewsPage().EnterTextCategoryLinkInTheSearchInput(GetNewsPage().GetTextCategoryLink());
            Assert.IsTrue( GetNewsPage().FirstResultArticleIsCorrect("Panorama: Lockdown UK"));

        }
    }
}
