﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System.Linq;

namespace BBCAutomationTesting.pages
{
    class NewsPage : BasePage
    {
        public NewsPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//button[@class='sign_in-exit']")]
        public IWebElement popUpSingIn;

        [FindsBy(How=How.XPath, Using = "//div[@data-entityid='container-top-stories#1']/a")]
        public IWebElement textHeaderArticle;
       
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'stories__tertiary')]//div/a")]
        public IWebElement textSecondaryTitleArticle;

        [FindsBy(How = How.XPath, Using = "//li[@class='nw-c-promo-meta']/a")]
        public IWebElement textCategoryLink;

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        public IWebElement inputSearch;

        [FindsBy(How = How.XPath, Using = "//p[contains(@class, 'PromoHeadline')]/a")]
        public IWebElement searchResultFirstArticle;

        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        public IWebElement clicksearchButton; 

        

        public void ClickOnThePopUpSingIn()
        {
            popUpSingIn.Click();
        }
        public string GetTextHeaderArticle()
        {
            return textHeaderArticle.Text;
        }

        public string[] GetSecondaryTitleArticle()
        {
            string[] actualElements = driver.FindElements(By.XPath("//div[contains(@class, 'stories__tertiary')]//div/a"))
               .Select(element => element.Text)
               .Where(s => !string.IsNullOrWhiteSpace(s))
               .Distinct()
               .ToArray();
            return actualElements;
        }

        public string GetTextCategoryLink()
        {
            return textCategoryLink.Text;
        }
        public void EnterTextCategoryLinkInTheSearchInput(string textcategory)
        {
            inputSearch.SendKeys(textcategory);
            clicksearchButton.Click();

        }
        public bool FirstResultArticleIsCorrect(string containsText)
        {
            return searchResultFirstArticle.Text.Contains(containsText);
        
        }
    }
}
