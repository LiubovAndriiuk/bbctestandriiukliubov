﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;


namespace BBCAutomationTesting.pages
{
    class BasePage
    {
        protected IWebDriver driver;
        protected WebDriverWait wait;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
           wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }
        public NewsPage GetHeader() => new NewsPage(driver);
    }
}
