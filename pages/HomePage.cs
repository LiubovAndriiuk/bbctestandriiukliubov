﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace BBCAutomationTesting.pages
{
    class HomePage : BasePage
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How =How.XPath,Using = "//div[@id='orb-nav-links']//a[contains(text(),'News')]")]
        public IWebElement newsHeading { get; set; }

        public void ClickOnTheNewsHeading()
        {
            newsHeading.Click();
        }
    }
}
